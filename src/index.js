import React from 'react'
import ReactDOM from 'react-dom/client'

// No.1
const items = [
  { text: 'Buy grocery', done: true },
  { text: 'Play guitar', done: false },
  { text: 'Romantic dinner', done: false },
]

const TodoItem = (props) => (
  <li onClick={props.onClick}>
    {props.item.text} - {props.item.done ? 'Done' : 'Ongoing'}
  </li>
)

class TodoList extends React.Component {
  render() {
    const { items, onListClick } = this.props
    return (
      <ul onClick={onListClick}>
        {items.map((item, index) => (
          <TodoItem
            item={item}
            key={index}
            onClick={this.handleItemClick.bind(this, item)}
          />
        ))}
      </ul>
    )
  }

  handleItemClick(item, event) {
    // Write your code here
    if (item.done) {
      return
    } else {
      this.props.onItemClick(item, event)
    }
  }
}

// No.2
class Input extends React.PureComponent {
  render() {
    let { forwardedRef, ...otherProps } = this.props
    return <input {...otherProps} ref={forwardedRef} />
  }
}

const TextInput = React.forwardRef((props, ref) => {
  return <Input {...props} forwardedRef={ref} />
})

class FocusableInput extends React.Component {
  ref = React.createRef()

  render() {
    return <TextInput ref={this.ref} />
  }

  // When the focused prop is changed from false to true,
  // and the input is not focused, it should receive focus.
  // If focused prop is true, the input should receive the focus.
  // Implement your solution below:
  componentDidUpdate(prevProps) {
    console.log('componentDidUpdate')
    console.log(this.props.focused)
    console.log(prevProps.focused)
    if (this.props.focused && !prevProps.focused) {
      this.ref.current.focus()
    }
  }

  componentDidMount() {
    console.log('componentDidMount')
    console.log(this.props.focused)
    if (this.props.focused) {
      this.ref.current.focus()
    }
  }
}

FocusableInput.defaultProps = {
  focused: false,
}

const App = (props) => (
  <>
    {/* No.1 */}
    <TodoList
      items={props.items}
      onListClick={(event) => console.log('List clicked!')}
      onItemClick={(item, event) => {
        console.log(item, event)
      }}
    />
    {/* No.2 */}
    <FocusableInput focused={props.focused} />
  </>
)

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <React.StrictMode>
    <App items={items} />
  </React.StrictMode>
)
